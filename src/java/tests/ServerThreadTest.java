package tests;

import client.ClientThread;
import client.ServerListThread;
import server.game_thread.GameThread;
import server.network_thread.ServerThread;
import snakes.SnakesProto;
import utils.ServerToJoin;
import utils.UDatagramSocket;
import utils.wrappers.Config;
import utils.wrappers.GameMessage;
import utils.wrappers.MessageType;
import utils.wrappers.message_wrappers.Join;

import java.io.IOException;
import java.net.*;

import static java.lang.Thread.sleep;

public class ServerThreadTest {
    public static void main(String[] args)  {
        try {
            UDatagramSocket player = new UDatagramSocket(new DatagramSocket(6000));
            UDatagramSocket player2 = new UDatagramSocket(new DatagramSocket(6001));

            Config config = new Config();
            config.setStateMs(1000);
            config.setHeight(10);
            config.setWidth(10);
            config.setDeadFoodProb(0.1f);
            config.setFoodPerPlayer(1);
            config.setFoodStatic(3);
            config.setPingMs(100);
            config.setTimeoutMs(5000);
            GameMessage joining = new GameMessage(MessageType.JOIN, 0L,
                    new Join(false, "Zheka"),
                    new InetSocketAddress(InetAddress.getLocalHost(), 7000));
            ServerThread st = new ServerThread(new UDatagramSocket(7000), config, null);
            Thread thread = new Thread(st);
            thread.start();

            /*ClientThread gt = new ClientThread("Zheka", 8000);
            sleep(5000);
            Thread n = new Thread(gt);
            gt.getMcThread().getCurrentGameList().forEach(o -> System.out.println(o.getServerAddress().toString()));
            gt.connect(gt.getMcThread().getCurrentGameList().get(0), false);
            n.start();
            //gt.connectToServer("Leha", false);
            /*sleep(5000);
            player.sendGMessage(joining);
            GameMessage answer = player.getGMessage();
            System.out.println(answer.getAddress().getHostString() + " " + answer.getMessageType().toString());
            sleep(5000);
            player2.sendGMessage(joining);
            answer = player2.getGMessage();
            System.out.println(answer.getAddress().getHostString() + " " + answer.getMessageType().toString());
            //sleep(10000000);*/
        } catch (IOException  e) {
            System.out.println(e.getMessage());
        }
    }
}
