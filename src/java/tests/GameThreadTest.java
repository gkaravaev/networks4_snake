package tests;

import server.game_thread.Direction;
import server.game_thread.GameThread;
import utils.MutableInt;
import utils.wrappers.Config;
import utils.wrappers.NodeRole;
import utils.wrappers.Player;
import utils.wrappers.State;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.InetAddress;

public class GameThreadTest {
    public static void main(String[] args) throws IOException, InterruptedException {
        Config config = new Config();
        config.setHeight(20);
        config.setWidth(20);
        config.setDeadFoodProb(1F);
        config.setStateMs(1000);
        config.setPingMs(1000);
        config.setTimeoutMs(1000);
        config.setFoodPerPlayer(0);
        config.setFoodStatic(3);
        State state = new State(config);
        // создаем игрока
        Player player = new Player();
        player.setName("Zheka");
        player.setIp(InetAddress.getLocalHost().getHostAddress());
        player.setPort(8000);
        player.setId(0);
        player.setRole(NodeRole.MASTER);
        // создаем игрока
        Player player2 = new Player();
        player2.setName("Zheka2");
        player2.setIp(InetAddress.getLocalHost().getHostAddress());
        player2.setPort(8001);
        player2.setId(1);
        player2.setRole(NodeRole.NORMAL);
        // змейка вида o--
        State.Snake snake = new State.Snake(Direction.UP);
        snake.getPoints().add(new State.Coord(9, 7));
        snake.getPoints().add(new State.Coord(1, 0));
        snake.getPoints().add(new State.Coord(1, 0));
        snake.setPlayerID(0);
        // змейка вида o--
        State.Snake snake1 = new State.Snake(Direction.RIGHT);
        snake1.getPoints().add(new State.Coord(0, 3));
        snake1.getPoints().add(new State.Coord(0, 1));
        snake1.getPoints().add(new State.Coord(0, 1));
        snake.setPlayerID(1);
        // создаем state
        state.getSnakes().add(snake);
        state.getSnakes().add(snake1);
        //state.getSnakes().add(snake1);
        state.getPlayers().add(player);
        state.getPlayers().add(player2);
        state.getFoods().add(new State.Coord(2, 7));
        //msgSeq (эмулируем сервер)
        MutableInt msqSeq = new MutableInt(0);
        GameThread gt = new GameThread(config, state, msqSeq, 7000);
        Thread gtt = new Thread(gt);
        gtt.start();
        // отображение
        /*JFrame frame = new JFrame("");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        frame.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyChar()) {
                    case 'w':
                        synchronized (state) {
                            state.steerSnake(0, Direction.UP);
                        }
                        break;
                    case 'd':
                        synchronized (state) {
                            state.steerSnake(0, Direction.RIGHT);
                        }
                        break;
                    case 's':
                        synchronized (state) {
                            state.steerSnake(0, Direction.DOWN);
                        }
                        break;
                    case 'a':
                        synchronized (state) {
                            state.steerSnake(0, Direction.LEFT);
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout(5, 5));
        GameFieldView view = new GameFieldView(gt);

        contentPane.add(view);

        frame.setContentPane(contentPane);
        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
        view.launchView();*/


        Thread.sleep(1000);
        int counter = 0;
        while (true) {
            counter++;
            if (counter % 5 == 0)
                switch (counter % 20) {
                    case 0:
                        snake.setDirection(Direction.UP);
                        break;
                    case 5:
                        snake.setDirection(Direction.LEFT);
                        break;
                    case 10:
                        snake.setDirection(Direction.DOWN);
                        break;
                    case 15:
                        snake.setDirection(Direction.RIGHT);
                        break;
                }
            Thread.sleep(1000);
        }


    }
}
