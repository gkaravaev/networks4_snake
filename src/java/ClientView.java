import client.ClientThread;
import utils.ServerToJoin;
import utils.wrappers.Config;
import utils.wrappers.Player;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class ClientView {

    private ClientThread client;

    ClientView(ClientThread client) {
        this.client = client;
    }

    private JFrame frame;
    private JPanel buttonPanel;
    private GameFieldView snakeView;
    private JTextArea scoreboard;
    private JScrollPane scoreboardPane;
    private JTextArea serverList;
    private JScrollPane serverListPane;
    private JTextArea serverChoose;

    private void drawDependingOnGame() {
        snakeView = new GameFieldView(client);
        Dimension gameSize = snakeView.getPreferredSize();
        int x = gameSize.width;
        int y = gameSize.height;
        //////gamefield
        frame.add(snakeView);
        snakeView.setBounds(0, 0, x, y);
        //////buttonpanel
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        JButton newGame = new JButton("New game");
        newGame.addActionListener(o -> client.newGame());
        JButton connect = new JButton("Connect");
        connect.addActionListener(o -> {
            int serverNumber = Integer.parseInt(serverChoose.getText());
            client.connect(client.getMcThread().getCurrentGameList().get(serverNumber), false);
        });
        JButton exit = new JButton("Exit");
        exit.addActionListener(o -> {
            if (client.getCurrentGameConfig() != null) {
                client.exit();
            }
        });
        buttonPanel.add(connect);
        buttonPanel.add(newGame);
        buttonPanel.add(exit);
        frame.add(buttonPanel);
        buttonPanel.setBounds(0, y, x, 40);
        //////connectpanel
        serverChoose = new JTextArea();
        JScrollPane writeHere = new JScrollPane(serverChoose);
        writeHere.setAutoscrolls(false);
        writeHere.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        writeHere.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        serverChoose.setLineWrap(true);
        serverChoose.addKeyListener(
                new KeyListener() {
                    @Override
                    public void keyTyped(KeyEvent e) {
                    }

                    @Override
                    public void keyPressed(KeyEvent e) {
                        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                            if (client.getCurrentGameConfig() != null) {
                                try {
                                    Integer.parseInt(serverChoose.getText());
                                } catch (NumberFormatException e1) {
                                    if (serverChoose.getText().length() == 1) {
                                        client.steerMySnake(serverChoose.getText().charAt(0));
                                    }
                                } finally {
                                    serverChoose.setText("");
                                }
                            }
                            e.consume();
                        }
                    }

                    @Override
                    public void keyReleased(KeyEvent e) {
                    }
                }
        );
        frame.add(serverChoose);
        serverChoose.setBounds(x, y + 40, x / 2, (y + 40) / 2);
        //////scoreboard
        scoreboard = new JTextArea();
        scoreboardPane = new JScrollPane(scoreboard);
        scoreboardPane.setAutoscrolls(false);
        scoreboardPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scoreboardPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scoreboard.setLineWrap(true);
        frame.add(scoreboardPane);
        scoreboard.setEditable(false);
        scoreboardPane.setBounds(x, 0, x / 2, y + 40);
        //////serverlist
        serverList = new JTextArea();
        serverListPane = new JScrollPane(serverList);
        serverListPane.setAutoscrolls(false);
        serverListPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        serverListPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        serverList.setLineWrap(true);
        frame.add(serverListPane);
        serverList.setEditable(false);
        serverListPane.setBounds(0, y + 40, x, (y + 40) / 2);
        frame.setSize((int) Math.round(x * 1.5), (int) Math.round((y + 40) * 1.5));

    }

    private void openGame() {
        this.frame = new JFrame();
        frame.setTitle("Snake");
        frame.setResizable(false);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        drawDependingOnGame();
    }

    private void redraw() {
        resizeEverything();
        snakeView.repaint();
        StringBuilder servers = new StringBuilder();
        int i = 0;
        synchronized (client.getMcThread().getCurrentGameList()) {
            for (ServerToJoin server : client.getMcThread().getCurrentGameList()) {
                servers.append(i).append(") ")
                        .append(server.getServerAddress().toString())
                        .append(" ")
                        .append(server.isCanJoin())
                        .append("\n");
                i++;
            }
        }
        serverList.setText(servers.toString());
        if (client.getCurrentState() != null) {
            StringBuilder players = new StringBuilder();
            for (Player player : client.getCurrentState().getPlayers()) {
                players.append(player.getName()).append(" ").append(player.getScore()).append("\n");
            }
            scoreboard.setText(players.toString());
        } else {
            scoreboard.setText("");
        }
    }

    private void resizeEverything() {
        Dimension gameSize = snakeView.getPreferredSize();
        int x = gameSize.width;
        int y = gameSize.height;
        snakeView.setBounds(0, 0, x, y);
        buttonPanel.setBounds(0, y, x, 40);
        serverChoose.setBounds(x, y + 40, x / 2, (y + 40) / 2);
        scoreboardPane.setBounds(x, 0, x / 2, y + 40);
        serverListPane.setBounds(0, y + 40, x, (y + 40) / 2);
        frame.setSize((int) Math.round(x * 1.5), (int) Math.round((y + 40) * 1.5));
    }

    public void launchClient() {
        openGame();
        frame.setVisible(true);
        while (true) {
            try {
                Thread.sleep(950);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            redraw();
        }
    }
}