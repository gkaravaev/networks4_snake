package client;

import server.game_thread.Direction;
import server.network_thread.ServerThread;
import utils.NotConfirmedMessage;
import utils.ServerToJoin;
import utils.UDatagramSocket;
import utils.wrappers.*;
import utils.wrappers.message_wrappers.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class ClientThread implements Runnable {
    private static final int MAXIMUM_PACKET_LENGTH = 64000;

    private ServerListThread serverChecker;
    private MulticastSocket mcServer;

    private UDatagramSocket mySocket;

    private boolean isServer;
    private Thread myServer;
    private ServerThread serverThread;

    private Config serverConfig;

    private boolean needToJoin;
    private ServerToJoin queriedServer;
    private boolean queriedToView;

    private boolean needToBecomeServer;

    private boolean needToExit;

    private String name;
    private InetSocketAddress currentServerAddress;
    private Config currentGameConfig;
    private State currentState;
    private HashMap<Long, NotConfirmedMessage> messageTimeout;
    private Long msgSeq;
    private boolean isPlaying;
    private boolean isDeputy;
    private int myID;

    public ClientThread(String name, int port, Config serverConfig) {
        try {
            this.serverConfig = serverConfig;
            needToExit = false;
            needToJoin = false;
            needToBecomeServer = false;
            this.name = name;
            this.messageTimeout = new HashMap<>();
            this.mySocket = new UDatagramSocket(port);
            this.mcServer = new MulticastSocket(9192);
            this.mcServer.joinGroup(InetAddress.getByName("239.192.0.4"));
            serverChecker = new ServerListThread(this.mcServer);
            new Thread(serverChecker).start();
            isServer = false;
            isDeputy = false;
            isPlaying = false;
            currentGameConfig = null;
            currentState = null;
            currentServerAddress = null;
            myID = 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ServerListThread getMcThread() {
        return serverChecker;
    }

    public Config getCurrentGameConfig() {
        return currentGameConfig;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void connect(ServerToJoin server, boolean onlyView) {
        queriedServer = server;
        queriedToView = onlyView;
        needToJoin = true;
    }

    private void connectToServer() {
        Config previousConfig = currentGameConfig;
        State previousState = currentState;
        try {
            mySocket.setTimeout(0);
            GameMessage joining = new GameMessage(MessageType.JOIN, 0L,
                    new Join(queriedToView, name), queriedServer.getServerAddress());
            mySocket.sendGMessage(joining);
            GameMessage answer = mySocket.getGMessage();
            System.out.println(answer.getMessageType());
            if (answer.getMessageType().equals(MessageType.ACK)) {
                currentGameConfig = queriedServer.getConfig();
                this.messageTimeout.clear();
                msgSeq = 0L;
                currentServerAddress = queriedServer.getServerAddress();
                if (!queriedToView) {
                    isPlaying = true;
                }
                myID = answer.getReceiverID();
                System.out.println("Connection successful!");
                mySocket.setTimeout(1000);
            } else {
                mySocket.setTimeout(1000);
                throw new ConnectException("Server refused to let me in!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            this.currentGameConfig = previousConfig;
            this.currentState = previousState;
        }
    }

    public void newGame(){
        needToBecomeServer = true;
    }

    private void becomeServer(Config config) {
        needToBecomeServer = false;
        if (isDeputy) {
            serverThread = new ServerThread(mySocket, config, currentState, this);
            isDeputy = false;
        } else {
            serverThread = new ServerThread(mySocket, config, this);
        }
        currentServerAddress = new InetSocketAddress(mySocket.getSocket().getLocalAddress(),
                mySocket.getSocket().getLocalPort());
        currentGameConfig = config;
        currentState = serverThread.getState();
        msgSeq = 0L;
        myServer = new Thread(serverThread);
        isServer = true;
        isPlaying = true;
        myServer.start();
    }

    private void stopBeingMaster() {
        serverThread.stopServer();
        isServer = false;
    }

    private void sendConfirmationMessage(GameMessage msgToConfirm, int receiverID) {
        GameMessage ackMsg = new GameMessage(MessageType.ACK,
                msgToConfirm.getMsgSeq(), myID, receiverID, new Ack(),
                msgToConfirm.getAddress());
        try {
            mySocket.sendGMessage(ackMsg);
        } catch (IOException e) {
            System.out.println("Failed to send confirmation message!");
        }
    }

    public void steerMySnake(char symbol) {
        Direction whereToSteer;
        switch (symbol) {
            case 'w':
                whereToSteer = Direction.UP;
                break;
            case 's':
                whereToSteer = Direction.DOWN;
                break;
            case 'a':
                whereToSteer = Direction.LEFT;
                break;
            case 'd':
                whereToSteer = Direction.RIGHT;
                break;
            default:
                return;
        }
        if (isPlaying) {
            if (!isServer) {
                GameMessage steerMsg = new GameMessage(MessageType.STEER, msgSeq,
                        myID, -1,
                        new Steer(whereToSteer), currentServerAddress);
                try {
                    System.out.println(currentServerAddress.getHostString());
                    mySocket.sendGMessage(steerMsg);
                    messageTimeout.put(msgSeq, new NotConfirmedMessage(steerMsg, new Date().getTime()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                msgSeq++;
            } else {
                synchronized (this.getCurrentState()) {
                    try {
                        currentState.steerSnake(myID, whereToSteer);
                    } catch (NoSuchElementException e) {
                        isPlaying = false;
                    }
                }
            }
        }
    }

    public void setIsPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public void exit(){
        needToExit = true;
    }

    private void exitServer() throws IOException {
        needToExit = false;
        if (isServer) {
            stopBeingMaster();
            currentServerAddress = null;
            currentGameConfig = null;
            currentState = null;
        } else {
            mySocket.setTimeout(0);
            GameMessage exit = new GameMessage(MessageType.ROLE, msgSeq,
                    myID, currentState.findIDofRole(NodeRole.MASTER),
                    new Role(NodeRole.VIEWER, NodeRole.MASTER), currentServerAddress);
            mySocket.sendGMessage(exit);
            currentServerAddress = null;
            currentGameConfig = null;
            currentState = null;
            mySocket.setTimeout(100);
        }
        isPlaying = false;
    }

    private void sendGameMessages() {
        try {
            if (currentGameConfig != null) {
                GameMessage ping = new GameMessage(MessageType.PING, msgSeq,
                        myID, currentState.findIDofRole(NodeRole.MASTER), new Ping(),
                        currentServerAddress);
                mySocket.sendGMessage(ping);
                messageTimeout.put(msgSeq, new NotConfirmedMessage(ping, new Date().getTime()));
                msgSeq++;

            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    // метод обработки сообщений, вызывается только если клиент не является сервером
    private void processMessage(GameMessage message) {
        switch (message.getMessageType()) {
            case ACK:
                messageTimeout.remove(message.getMsgSeq());
                break;
            case STATE:
                currentState = ((MState) message.getMsgContent()).getState();
                sendConfirmationMessage(message, message.getSenderID());
                break;
            case ROLE:
                Role roleMessage = ((Role) message.getMsgContent());
                if (currentState != null && roleMessage.getSenderRole().equals(NodeRole.MASTER)
                        && message.getSenderID() == currentState.findIDofRole(NodeRole.DEPUTY)) {
                    // 1. от заместителя другим игрокам о том, что пора начинать считать его главным (sender_role = MASTER)
                    messageTimeout.clear();
                    currentServerAddress = message.getAddress();
                } else if (roleMessage.getReceiverRole().equals(NodeRole.VIEWER)) {
                    // 3. от главного к умершему игроку (receiver_role = VIEWER)
                    isPlaying = false;
                }
                if (roleMessage.getReceiverRole().equals(NodeRole.DEPUTY)) {
                    //4. в комбинации с 1,2 или отдельно от них: назначение кого-то заместителем (receiver_role = DEPUTY)
                    isDeputy = true;
                }
                if (roleMessage.getSenderRole().equals(NodeRole.VIEWER)
                        && roleMessage.getReceiverRole().equals(NodeRole.MASTER)) {
                    // 5. в комбинации с 2 от главного узла заместителю о том, что он становится главным (receiver_role = MASTER)
                    becomeServer(currentGameConfig);
                }
                sendConfirmationMessage(message, message.getSenderID());
                break;
        }
    }

    private void checkFlags(){
        if (needToJoin) {
            needToJoin = false;
            connectToServer();
            queriedToView = false;
            queriedServer = null;
        }
        if (needToBecomeServer) {
            becomeServer(new Config());
        }
        if (needToExit) {
            try {
                exitServer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkTimeout(){
        for (Map.Entry<Long, NotConfirmedMessage> m : messageTimeout.entrySet()) {
            if (new Date().getTime() - m.getValue().getTime() > currentGameConfig.getTimeoutMs()) {
                becomeServer(currentGameConfig);
            }
        }
    }

    public int getMyID() {
        return myID;
    }

    @Override
    public void run() {
        try {
            mySocket.setTimeout(100);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        while (true) {
            checkFlags();
            if (!isServer) {
                try {
                    GameMessage message = mySocket.getGMessage();
                    System.out.println(message.getMessageType());
                    if (message.getAddress().equals(currentServerAddress)
                            || (message.getMessageType().equals(MessageType.ROLE) && currentState != null && message.getSenderID() == currentState.findIDofRole(NodeRole.DEPUTY))) {
                        processMessage(message);
                    }
                } catch (SocketTimeoutException e) {
                    if (currentServerAddress != null) {
                        sendGameMessages();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (isDeputy)
                    checkTimeout();
            } else {
                currentState = serverThread.getState();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
