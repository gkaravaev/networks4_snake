package client;

import utils.ServerToJoin;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.*;

public class ServerListThread implements Runnable {

    private static final int SERVER_TIMEOUT = 1000;
    private MulticastSocket server;
    private HashMap<ServerToJoin, Long> currentGameList;

    ServerListThread(MulticastSocket server) {
        this.server = server;
        try {
            server.setSoTimeout(1000);
        } catch (SocketException e) {
            System.out.println("Multicast socket timeout can't be set somewhy!");
        }
        this.currentGameList = new HashMap<>();
    }

    public ArrayList<ServerToJoin> getCurrentGameList() {
        return new ArrayList<>(currentGameList.keySet());
    }

    public void removeOnTimeout(){
        ArrayList<ServerToJoin> toRemove = new ArrayList<>();
        synchronized (currentGameList) {
            for (Map.Entry<ServerToJoin, Long> s : currentGameList.entrySet()) {
                if (new Date().getTime() - s.getValue() > SERVER_TIMEOUT) {
                    toRemove.add(s.getKey());
                }
            }
            for (ServerToJoin rd : toRemove) {
                currentGameList.remove(rd);
            }
        }
    }

    @Override
    public void run() {
        byte[] msg = new byte[10000];
        DatagramPacket serverMsg = new DatagramPacket(msg, 10000);
        while (true) {
            try {
                server.receive(serverMsg);
                ServerToJoin newServer = new ServerToJoin(serverMsg);
                synchronized (currentGameList) {
                    currentGameList.put(newServer, new Date().getTime());
                }
            } catch (SocketTimeoutException e1) {
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                synchronized (currentGameList) {
                    removeOnTimeout();
                }
            }
        }
    }

}
