
import client.ClientThread;
import server.game_thread.GameThread;
import utils.wrappers.State;

import java.awt.*;
import javax.swing.*;


public class GameFieldView extends JPanel {

    private ClientThread game;

    GameFieldView(ClientThread client){
        game = client;
    }


    public void launchView() throws InterruptedException {
        while (true) {
            repaint();
            Thread.sleep(500);
        }
    }

    @Override
    public Dimension getPreferredSize () {
        if (game.getCurrentGameConfig() != null) {
            return new Dimension(this.game.getCurrentGameConfig().getWidth() * 10,
                    this.game.getCurrentGameConfig().getHeight() * 10);
        } else {
            return new Dimension(300, 200);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        if (game.getCurrentGameConfig() != null) {
            g.fillRect(0, 0, this.game.getCurrentGameConfig().getWidth() * 10,
                    this.game.getCurrentGameConfig().getHeight() * 10);
            if (game.getCurrentState() != null) {
                State state = game.getCurrentState();
                synchronized (state) {
                    for (int j = 0; j < state.getConfig().getHeight(); j++) {
                        for (int i = 0; i < state.getConfig().getWidth(); i++) {
                            switch (state.getField()[i][j]) {
                                case Empty:
                                    break;
                                case Snake:
                                    g.setColor(Color.YELLOW);
                                    g.fillRect(i * 10, j * 10, 10, 10);
                                    break;
                                case Food:
                                    g.setColor(Color.RED);
                                    g.fillRect(i * 10, j * 10, 10, 10);
                                    break;
                            }
                        }
                    }
                }
            }
        } else {
            g.fillRect(0, 0, 300, 200);
        }


    }
}