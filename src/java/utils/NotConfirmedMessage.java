package utils;

import snakes.SnakesProto;
import utils.wrappers.GameMessage;

public class NotConfirmedMessage {
    private GameMessage message;
    private Long time;
    public NotConfirmedMessage (GameMessage msg, Long time) {
        this.message = msg;
        this.time = time;
    }

    public GameMessage getMessage() {
        return message;
    }

    public void setMessage(GameMessage message) {
        this.message = message;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
