package utils.wrappers;

import snakes.SnakesProto;

public enum NodeRole {
    NORMAL, MASTER, DEPUTY, VIEWER;

    public SnakesProto.NodeRole toProtobuf() {
        switch (this) {
            case DEPUTY:
                return SnakesProto.NodeRole.DEPUTY;
            case NORMAL:
                return SnakesProto.NodeRole.NORMAL;
            case MASTER:
                return SnakesProto.NodeRole.MASTER;
            case VIEWER:
                return SnakesProto.NodeRole.VIEWER;
            default:
                return SnakesProto.NodeRole.NORMAL;
        }
    }

    public static NodeRole fromProtobuf(SnakesProto.NodeRole pb) {
        switch (pb) {
            case NORMAL:
                return NORMAL;
            case MASTER:
                return MASTER;
            case DEPUTY:
                return DEPUTY;
            case VIEWER:
                return VIEWER;
            default:
                return NORMAL;
        }
    }
}
