package utils.wrappers;


import server.game_thread.Cell;
import server.game_thread.Direction;
import snakes.SnakesProto;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Random;

public class State {

    public static class Coord {
        private int x;
        private int y;

        public Coord() {
        }

        public Coord(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public boolean equals(Coord obj) {
            return this.x == obj.getX() && this.y == obj.getY();
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public SnakesProto.GameState.Coord toProtobuf() {
            return SnakesProto.GameState.Coord.newBuilder().setX(this.x).setY(this.y).build();
        }
    }

    public static class Snake {

        private int playerID;
        private ArrayList<Coord> points;
        private SnakesProto.GameState.Snake.SnakeState state;
        private Direction direction;

        public Snake(Direction direction) {
            this.direction = direction;
            this.points = new ArrayList<>();
            this.state = SnakesProto.GameState.Snake.SnakeState.ALIVE;
        }

        public Snake(SnakesProto.GameState.Snake pb) {
            this.playerID = pb.getPlayerId();
            this.direction = Direction.fromProtobuf(pb.getHeadDirection());
            this.points = new ArrayList<>();
            this.state = pb.getState();
            for (SnakesProto.GameState.Coord c : pb.getPointsList()) {
                points.add(new Coord(c.getX(), c.getY()));
            }

        }

        public Snake(Direction direction, int playerID) {
            this.direction = direction;
            this.playerID = playerID;
            this.points = new ArrayList<>();
            this.state = SnakesProto.GameState.Snake.SnakeState.ALIVE;
        }

        public Direction getDirection() {
            return direction;
        }

        public void setDirection(Direction direction) {
            this.direction = direction;
        }

        public int getPlayerID() {
            return playerID;
        }

        public void setPlayerID(int playerID) {
            this.playerID = playerID;
        }

        public ArrayList<Coord> getPoints() {
            return points;
        }

        public void setPoints(ArrayList<Coord> points) {
            this.points = points;
        }

        public SnakesProto.GameState.Snake.SnakeState getState() {
            return state;
        }

        public void setState(SnakesProto.GameState.Snake.SnakeState state) {
            this.state = state;
        }

        public SnakesProto.GameState.Snake toProtobuf() {
            ArrayList<SnakesProto.GameState.Coord> pbCoordList = new ArrayList<>();
            for (Coord c : points) pbCoordList.add(c.toProtobuf());
            return SnakesProto.GameState.Snake.newBuilder()
                    .setHeadDirection(direction.toProtobuf())
                    .setPlayerId(this.playerID)
                    .setState(this.state)
                    .addAllPoints(pbCoordList)
                    .build();
        }
    }

    private int stateOrder;
    private ArrayList<Snake> snakes;
    private ArrayList<Coord> foods;
    private ArrayList<Player> players;
    private Config config;
    private Cell[][] field;

    public State() {
        stateOrder = 0;
        snakes = new ArrayList<>();
        foods = new ArrayList<>();
        players = new ArrayList<>();
        config = new Config();
        field = new Cell[config.getWidth()][config.getHeight()];
    }

    public State(Config config) {
        stateOrder = 0;
        snakes = new ArrayList<>();
        foods = new ArrayList<>();
        players = new ArrayList<>();
        this.config = config;
        field = new Cell[config.getWidth()][config.getHeight()];
    }

    public State(SnakesProto.GameState pb) {
        stateOrder = 0;
        snakes = new ArrayList<>();
        foods = new ArrayList<>();
        players = new ArrayList<>();
        config = new Config(pb.getConfig());
        field = new Cell[config.getWidth()][config.getHeight()];
        for (int i = 0; i < config.getWidth(); i++) {
            for (int j = 0; j < config.getHeight(); j++) {
                field[i][j] = Cell.Empty;
            }
        }
        for (SnakesProto.GameState.Snake snake : pb.getSnakesList()) {
            Snake snake1 = new Snake(snake);
            snakes.add(snake1);
            drawSnake(snake1);
        }
        for (SnakesProto.GamePlayer p : pb.getPlayers().getPlayersList()) {
            players.add(new Player(p));
        }
        for (SnakesProto.GameState.Coord c : pb.getFoodsList()) {
            foods.add(new Coord(c.getX(), c.getY()));
            field[c.getX()][c.getY()] = Cell.Food;
        }
    }

    public void kickPlayer(int id) {
        try {
            getPlayerByID(id);
            players.remove(getPlayerByID(id));
            getSnakeByID(id).setState(SnakesProto.GameState.Snake.SnakeState.ZOMBIE);
            getSnakeByID(id).setPlayerID(-100);
            System.out.println("Kicked player with id " + id);
        } catch (NoSuchElementException e) {
            System.out.println(e.getMessage());
        }
    }

    private boolean isSquareAvailable(Coord upperLeft) {
        for (int x, i = 0; i < 5; i++) {
            for (int y, j = 0; j < 5; j++) {
                x = (upperLeft.getX() + i) % config.getWidth();
                y = (upperLeft.getY() + j) % config.getHeight();
                if (x >= config.getWidth())
                    x = x % config.getWidth();
                if (y >= config.getHeight())
                    y = y % config.getHeight();
                if (field[x][y] == Cell.Snake) return false;
            }
        }
        return true;
    }

    public int findIDofRole(NodeRole role){
        for (Player p : getPlayers()) {
            if (p.getRole().equals(role)) {
                return p.getId();
            }
        }
        throw new NoSuchElementException("Such role not found!");
    }

    public boolean isDeputyExists(){
        for (Player p : getPlayers()) {
            if (p.getRole().equals(NodeRole.DEPUTY))
                return true;
        }
        return false;
    }

    private void drawSnake(Snake snake){
        int x = 0;
        int y = 0;
        for (Coord c : snake.getPoints()) {
            x = ((x + c.getX()) + config.getWidth()) % config.getWidth();
            y = ((y + c.getY()) + config.getHeight()) % config.getHeight();
            field[x][y] = Cell.Snake;
        }
    }

    public Coord findPlaceForNewSnake(){
        for (int x = 0; x < config.getWidth(); x++) {
            for (int y = 0; y < config.getHeight(); y++) {
                if (isSquareAvailable(new Coord(x, y))) {
                    return new Coord(x, y);
                }
            }
        }
        throw new NoSuchElementException("No place on field!");
    }

    public void spawnSnakeInSquare(Coord upperLeft, int playerID) {
        int direction = new Random().nextInt(4); // up, right, down, left
        Direction dir;
        if (direction == 0) dir = Direction.UP;
        else if (direction == 1) dir = Direction.RIGHT;
        else if (direction == 2) dir = Direction.DOWN;
        else dir = Direction.LEFT;
        Snake snake = new Snake(dir, playerID);

        snake.points.add(new Coord((upperLeft.getX() + 2) % config.getWidth(),
                (upperLeft.getY() + 2) % config.getHeight()));
        snake.points.add(dir.getReverse().toCoord());
        this.getSnakes().add(snake);
        field[snake.points.get(0).getX()][snake.points.get(0).getY()] = Cell.Snake;
        field[snake.points.get(0).getX() + snake.points.get(1).getX()][snake.points.get(0).getY() + snake.points.get(1).getY()] = Cell.Snake;
    }

    public Player findPlayerByIP(InetSocketAddress playerIP) {
        for (Player p : getPlayers()) {
            if (p.getIp().equals(playerIP.getAddress().getHostAddress()) &&
                    p.getPort() == playerIP.getPort()) {
                return p;
            }
        }
        throw new NoSuchElementException("Player not found!");
    }

    public Player getPlayerByID(int id) {
        for (Player p : getPlayers()) {
            if (p.getId() == id)
                return p;
        }
        throw new NoSuchElementException("Player with id " + id +  " not found!");
    }

    public Snake getSnakeByID(int id) {
        for (Snake s: getSnakes()) {
            if (s.getPlayerID() == id)
                return s;
        }
        throw new NoSuchElementException("Snake with id " + id +  " not found!");
    }

    public void steerSnake(int playerID, Direction direction){
        Snake snake = getSnakeByID(playerID);
        // направление можно поставить любое кроме противоположного текущему
        if (!direction.equals(snake.getDirection().getReverse()))
            snake.setDirection(direction);
    }

    public Cell[][] getField() {
        return field;
    }

    public void setField(Cell[][] field) {
        this.field = field;
    }

    public int getStateOrder() {
        return stateOrder;
    }

    public void setStateOrder(int stateOrder) {
        this.stateOrder = stateOrder;
    }

    public ArrayList<Snake> getSnakes() {
        return snakes;
    }

    public void setSnakes(ArrayList<Snake> snakes) {
        this.snakes = snakes;
    }

    public ArrayList<Coord> getFoods() {
        return foods;
    }

    public void setFoods(ArrayList<Coord> foods) {
        this.foods = foods;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public SnakesProto.GamePlayers getPBPlayers() {
        ArrayList<SnakesProto.GamePlayer> playersList = new ArrayList<>();
        for (Player player : getPlayers()) {
            playersList.add(player.toProtobuf());
        }
        return SnakesProto.GamePlayers.newBuilder().addAllPlayers(playersList).build();
    }

    public SnakesProto.GameState toProtobuf() {
        ArrayList<SnakesProto.GamePlayer> pbPlayers = new ArrayList<>();
        for (Player p : players) pbPlayers.add(p.toProtobuf());
        SnakesProto.GamePlayers gamePlayers = SnakesProto.GamePlayers.newBuilder().addAllPlayers(pbPlayers).build();

        ArrayList<SnakesProto.GameState.Coord> pbFoodList = new ArrayList<>();
        for (Coord c : foods) pbFoodList.add(c.toProtobuf());

        ArrayList<SnakesProto.GameState.Snake> pbSnakeList = new ArrayList<>();
        for (Snake s : snakes) pbSnakeList.add(s.toProtobuf());

        return SnakesProto.GameState.newBuilder()
                .addAllFoods(pbFoodList)
                .addAllSnakes(pbSnakeList)
                .setPlayers(gamePlayers)
                .setConfig(this.config.toProtobuf())
                .setStateOrder(this.stateOrder)
                .build();

    }
}
