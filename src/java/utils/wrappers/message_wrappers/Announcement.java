package utils.wrappers.message_wrappers;

import snakes.SnakesProto;
import utils.wrappers.Config;
import utils.wrappers.Player;

import java.util.ArrayList;

public class Announcement implements Message {
    private ArrayList<Player> players;
    private Config config;
    private boolean canJoin;

    public Announcement(ArrayList<Player> players, Config config, boolean canJoin) {
        this.players = players;
        this.config = config;
        this.canJoin = canJoin;
    }

    public Announcement(SnakesProto.GameMessage.AnnouncementMsg pb) {
        for (SnakesProto.GamePlayer pl : pb.getPlayers().getPlayersList()) {
            players.add(new Player(pl));
        }
        this.config = new Config(pb.getConfig());
        this.canJoin = pb.getCanJoin();
    }

    public SnakesProto.GameMessage.AnnouncementMsg toPB(){
        ArrayList<SnakesProto.GamePlayer> players = new ArrayList<>();
        for (Player p : this.players) {
            players.add(p.toProtobuf());
        }
        return SnakesProto.GameMessage.AnnouncementMsg.newBuilder()
                .setCanJoin(canJoin)
                .setConfig(config.toProtobuf())
                .setPlayers(SnakesProto.GamePlayers.newBuilder().addAllPlayers(players))
                .build();
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public boolean isCanJoin() {
        return canJoin;
    }

    public void setCanJoin(boolean canJoin) {
        this.canJoin = canJoin;
    }
}
