package utils.wrappers.message_wrappers;

import server.game_thread.Direction;
import snakes.SnakesProto;

public class Steer implements Message {
    Direction direction;
    public Steer(Direction direction) {
        this.direction = direction;
    }

    public Steer(SnakesProto.GameMessage.SteerMsg pb) {
        this.direction = Direction.fromProtobuf(pb.getDirection());
    }
    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public SnakesProto.GameMessage.SteerMsg toPB() {
        return SnakesProto.GameMessage.SteerMsg.newBuilder()
                        .setDirection(direction.toProtobuf())
                        .build();
    }
}
