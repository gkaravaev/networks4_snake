package utils.wrappers.message_wrappers;

import snakes.SnakesProto;

public class Ack implements Message {
    public Ack(){}

    public SnakesProto.GameMessage.AckMsg toPB() {
        return SnakesProto.GameMessage.AckMsg.newBuilder().build();
    }
}
