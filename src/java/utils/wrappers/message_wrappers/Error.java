package utils.wrappers.message_wrappers;

import snakes.SnakesProto;

public class Error implements Message {
    private String errorMessage;

    public Error(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    public Error(SnakesProto.GameMessage.ErrorMsg pb) {
        this.errorMessage = pb.getErrorMessage();
    }

    public SnakesProto.GameMessage.ErrorMsg toPB(){
        return SnakesProto.GameMessage.ErrorMsg.newBuilder()
                .setErrorMessage(errorMessage)
                .build();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
