package utils.wrappers.message_wrappers;

import snakes.SnakesProto;

public class Ping implements Message {

    public SnakesProto.GameMessage.PingMsg toPB() {
        return SnakesProto.GameMessage.PingMsg.newBuilder().build();
    }
}
