package utils.wrappers.message_wrappers;

import snakes.SnakesProto;

public class Join implements Message {

    private boolean onlyView;
    private String name;

    public Join(boolean onlyView, String name) {
        this.onlyView = onlyView;
        this.name = name;
    }
    public Join(SnakesProto.GameMessage.JoinMsg pb){
        this.onlyView = pb.getOnlyView();
        this.name = pb.getName();
    }

    public SnakesProto.GameMessage.JoinMsg toPB(){
        return SnakesProto.GameMessage.JoinMsg.newBuilder()
                .setOnlyView(onlyView)
                .setName(name)
                .build();
    }

    public boolean isOnlyView() {
        return onlyView;
    }

    public void setOnlyView(boolean onlyView) {
        this.onlyView = onlyView;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
