package utils.wrappers.message_wrappers;

import snakes.SnakesProto;
import utils.wrappers.State;

public class MState implements Message{
    private utils.wrappers.State state;
    public MState(utils.wrappers.State state) {
        this.state = state;
    }
    public MState(SnakesProto.GameMessage.StateMsg pb) {
        this.state = new utils.wrappers.State(pb.getState());
    }
    public SnakesProto.GameMessage.StateMsg toPB(){
        return SnakesProto.GameMessage.StateMsg.newBuilder().setState(state.toProtobuf()).build();
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
