package utils.wrappers.message_wrappers;

import snakes.SnakesProto;
import utils.wrappers.NodeRole;

public class Role implements Message{
    private NodeRole senderRole;
    private NodeRole receiverRole;

    public Role(NodeRole senderRole, NodeRole receiverRole) {
        this.receiverRole = receiverRole;
        this.senderRole = senderRole;
    }

    public Role(SnakesProto.GameMessage.RoleChangeMsg pb){
        this.senderRole = NodeRole.fromProtobuf(pb.getSenderRole());
        this.receiverRole = NodeRole.fromProtobuf(pb.getReceiverRole());
    }

    public SnakesProto.GameMessage.RoleChangeMsg toPB(){
        return SnakesProto.GameMessage.RoleChangeMsg.newBuilder()
                .setReceiverRole(receiverRole.toProtobuf())
                .setSenderRole(senderRole.toProtobuf())
                .build();
    }

    public NodeRole getSenderRole() {
        return senderRole;
    }

    public void setSenderRole(NodeRole senderRole) {
        this.senderRole = senderRole;
    }

    public NodeRole getReceiverRole() {
        return receiverRole;
    }

    public void setReceiverRole(NodeRole receiverRole) {
        this.receiverRole = receiverRole;
    }
}
