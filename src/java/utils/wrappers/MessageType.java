package utils.wrappers;

import snakes.SnakesProto;

public enum MessageType {
    PING, STEER, ACK, STATE, ANNOUNCEMENT, JOIN, ERROR, ROLE;

    public static MessageType fromPB(SnakesProto.GameMessage pb) {
        if (pb.hasAck()) return ACK;
        else if (pb.hasPing()) return PING;
        else if (pb.hasSteer()) return STEER;
        else if (pb.hasState()) return STATE;
        else if (pb.hasAnnouncement()) return ANNOUNCEMENT;
        else if (pb.hasJoin()) return JOIN;
        else if (pb.hasError()) return ERROR;
        else return ROLE;
    }
}
