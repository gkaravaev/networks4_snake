package utils.wrappers;

import snakes.SnakesProto;
import utils.wrappers.message_wrappers.Join;

public class Player {

    private String name;
    private int id;
    private String ip;
    private int port;
    private NodeRole role;
    private int score;

    public Player(){
        this.score = 0;
        this.role = NodeRole.NORMAL;
    }

    public Player(GameMessage message, int newID) {
        this.name = ((Join) message.getMsgContent()).getName();
        this.ip = message.getAddress().getAddress().getHostAddress();
        this.port = message.getAddress().getPort();
        this.score = 0;
        this.id = newID;
        this.role = NodeRole.NORMAL;
    }

    public Player(SnakesProto.GamePlayer pl) {
        this.name = pl.getName();
        this.id = pl.getId();
        this.ip = pl.getIpAddress();
        this.port = pl.getPort();
        this.role = NodeRole.fromProtobuf(pl.getRole());
        this.score = pl.getScore();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public NodeRole getRole() {
        return role;
    }

    public void setRole(NodeRole role) {
        this.role = role;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public SnakesProto.GamePlayer toProtobuf(){
        return SnakesProto.GamePlayer.newBuilder()
                .setId(this.id)
                .setIpAddress(this.ip)
                .setName(this.name)
                .setPort(this.port)
                .setRole(this.role.toProtobuf())
                .setScore(this.score)
                .build();
    }
}
