package utils.wrappers;

import com.google.protobuf.InvalidProtocolBufferException;
import snakes.SnakesProto;
import utils.wrappers.message_wrappers.*;
import utils.wrappers.message_wrappers.Error;

import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.Arrays;

public class GameMessage {
    private InetSocketAddress address;
    private MessageType messageType;
    private Long msgSeq;
    private Integer senderID;
    private Integer receiverID;
    private Message msgContent;

    public GameMessage(MessageType messageType, Long msgSeq, Integer senderID,
                Integer receiverID, Message msg, InetSocketAddress address) {
        this.messageType = messageType;
        this.msgSeq = msgSeq;
        this.senderID = senderID;
        this.receiverID = receiverID;
        this.msgContent = msg;
        this.address = address;
    }
    public GameMessage(MessageType messageType, Long msgSeq, Message msg, InetSocketAddress address) {
        this.messageType = messageType;
        this.msgSeq = msgSeq;
        this.senderID = -1;
        this.receiverID = -1;
        this.msgContent = msg;
        this.address = address;
    }

    public GameMessage(DatagramPacket msg) throws InvalidProtocolBufferException {
        this.address = new InetSocketAddress(msg.getAddress(), msg.getPort());
        SnakesProto.GameMessage pb = SnakesProto.GameMessage.parseFrom(Arrays.copyOf(msg.getData(), msg.getLength()));
        this.msgSeq = pb.getMsgSeq();
        if (pb.hasSenderId())
            senderID = pb.getSenderId();
        else
            senderID = -1;
        if (pb.hasReceiverId())
            receiverID = pb.getReceiverId();
        else
            receiverID = -1;
        this.messageType = MessageType.fromPB(pb);
        switch (messageType) {
            case PING:
                this.msgContent = new Ping();
                break;
            case STEER:
                this.msgContent = new Steer(pb.getSteer());
                break;
            case ACK:
                this.msgContent = new Ack();
                break;
            case STATE:
                this.msgContent = new MState(pb.getState());
                break;
            case ANNOUNCEMENT:
                this.msgContent = new Announcement(pb.getAnnouncement());
                break;
            case JOIN:
                this.msgContent = new Join(pb.getJoin());
                break;
            case ERROR:
                this.msgContent = new Error(pb.getError());
                break;
            case ROLE:
                this.msgContent = new Role(pb.getRoleChange());
                break;
        }
    }

    public SnakesProto.GameMessage toPB() {
        SnakesProto.GameMessage.Builder msg = SnakesProto.GameMessage.newBuilder()
                .setMsgSeq(this.msgSeq);
        if (senderID != -1)
            msg.setSenderId(senderID);
        if (receiverID != -1)
            msg.setReceiverId(receiverID);
        switch (messageType) {
            case PING:
                msg.setPing(((Ping)msgContent).toPB());
                break;
            case STEER:
                msg.setSteer(((Steer)msgContent).toPB());
                break;
            case ACK:
                msg.setAck(((Ack)msgContent).toPB());
                break;
            case STATE:
                msg.setState(((MState)msgContent).toPB());
                break;
            case ANNOUNCEMENT:
                msg.setAnnouncement(((Announcement)msgContent).toPB());
                break;
            case JOIN:
                msg.setJoin(((Join)msgContent).toPB());
                break;
            case ERROR:
                msg.setError(((Error)msgContent).toPB());
                break;
            case ROLE:
                msg.setRoleChange(((Role)msgContent).toPB());
                break;
        }
        return msg.build();
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void setAddress(InetSocketAddress address) {
        this.address = address;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public Long getMsgSeq() {
        return msgSeq;
    }

    public void setMsgSeq(Long msgSeq) {
        this.msgSeq = msgSeq;
    }

    public Integer getSenderID() {
        return senderID;
    }

    public void setSenderID(Integer senderID) {
        this.senderID = senderID;
    }

    public Integer getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(Integer receiverID) {
        this.receiverID = receiverID;
    }

    public Message getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(Message msgContent) {
        this.msgContent = msgContent;
    }
}
