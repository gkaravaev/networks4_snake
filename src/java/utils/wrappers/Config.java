package utils.wrappers;

import snakes.SnakesProto;

public class Config {
    private int width;
    private int height;
    private int foodStatic;
    private double foodPerPlayer;
    private int stateMs;
    private int pingMs;
    private int timeoutMs;
    private double deadFoodProb;

    public Config() {
        this.width = 40;
        this.height = 30;
        this.foodStatic = 1;
        this.foodPerPlayer = 1;
        this.stateMs = 1000;
        this.pingMs = 100;
        this.timeoutMs = 10000;
        this.deadFoodProb = 0.1F;
    }

    public Config(SnakesProto.GameConfig config) {
        this.width = config.getWidth();
        this.height = config.getHeight();
        this.foodPerPlayer = config.getFoodPerPlayer();
        this.foodStatic = config.getFoodStatic();
        this.stateMs = config.getStateDelayMs();
        this.pingMs = config.getPingDelayMs();
        this.timeoutMs = config.getNodeTimeoutMs();
        this.deadFoodProb = config.getDeadFoodProb();
    }

    public int getStateMs() {
        return stateMs;
    }

    public void setStateMs(int stateMs) {
        this.stateMs = stateMs;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getFoodStatic() {
        return foodStatic;
    }

    public void setFoodStatic(int foodStatic) {
        this.foodStatic = foodStatic;
    }

    public double getFoodPerPlayer() {
        return foodPerPlayer;
    }

    public void setFoodPerPlayer(double foodPerPlayer) {
        this.foodPerPlayer = foodPerPlayer;
    }

    public double getDeadFoodProb() {
        return deadFoodProb;
    }

    public void setDeadFoodProb(double deadFoodProb) {
        this.deadFoodProb = deadFoodProb;
    }

    public int getPingMs() {
        return pingMs;
    }

    public void setPingMs(int pingMs) {
        this.pingMs = pingMs;
    }

    public int getTimeoutMs() {
        return timeoutMs;
    }

    public void setTimeoutMs(int timeoutMs) {
        this.timeoutMs = timeoutMs;
    }

    public SnakesProto.GameConfig toProtobuf() {
        return SnakesProto.GameConfig.newBuilder()
                .setDeadFoodProb((float) this.deadFoodProb)
                .setNodeTimeoutMs(this.timeoutMs)
                .setPingDelayMs(this.pingMs)
                .setFoodPerPlayer((float) this.foodPerPlayer)
                .setFoodStatic(this.foodStatic)
                .setStateDelayMs(this.stateMs)
                .setHeight(this.height)
                .setWidth(this.width)
                .build();
    }
}
