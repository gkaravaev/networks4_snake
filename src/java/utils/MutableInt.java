package utils;

import java.util.concurrent.atomic.AtomicInteger;

public class MutableInt {

    private AtomicInteger anInt;

    public MutableInt(){
        anInt = new AtomicInteger(0);
    }
    public MutableInt(int anInt) {
        this.anInt = new AtomicInteger(anInt);
    }

    public int getInt() {
        return anInt.intValue();
    }

    public void setInt(int anInt) {
         this.anInt.set(anInt);
    }

    public void incInt(int inc) {
        this.anInt.addAndGet(inc);
    }
}
