package utils;

import com.google.protobuf.AbstractMessage;
import snakes.SnakesProto;
import utils.wrappers.GameMessage;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.net.*;

public class UDatagramSocket {
    private DatagramSocket socket;

    public DatagramSocket getSocket() {
        return socket;
    }

    public UDatagramSocket(InetAddress ip, int port) {
        try {
            this.socket = new DatagramSocket(port, ip);
        } catch (SocketException e) {
            System.out.println("Creation of datagram socket went wrong!");
        }
    }
    public UDatagramSocket(int port) {
        try {
            this.socket = new DatagramSocket(port);
        } catch (SocketException e) {
            System.out.println("Creation of datagram socket went wrong!");
        }
    }

    public void setTimeout(int msec) throws SocketException {
        this.socket.setSoTimeout(msec);
    }

    public UDatagramSocket(DatagramSocket socket) {
        this.socket = socket;
    }

    public void sendGMessage(GameMessage msg) throws IOException {
        DatagramPacket msgToSend = new DatagramPacket(msg.toPB().toByteArray(),
                msg.toPB().toByteArray().length, msg.getAddress().getAddress(), msg.getAddress().getPort());
        socket.send(msgToSend);
    }

    private DatagramPacket getPacket() throws IOException{
        DatagramPacket msg = new DatagramPacket(new byte[64000], 64000);
        socket.receive(msg);
        return msg;
    }

    public GameMessage getGMessage() throws IOException {
        DatagramPacket msg = getPacket();
        return new GameMessage(msg);
    }
}
