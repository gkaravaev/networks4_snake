package utils;

import com.google.protobuf.InvalidProtocolBufferException;
import snakes.SnakesProto;
import utils.wrappers.Config;
import utils.wrappers.Player;

import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;

public class ServerToJoin {
    private ArrayList<Player> players;
    private Config config;
    private boolean canJoin;
    private InetSocketAddress serverAddress;
    public ServerToJoin(DatagramPacket msg) {
        try {
            SnakesProto.GameMessage msgPB = SnakesProto.GameMessage
                    .parseFrom(Arrays.copyOf(msg.getData(), msg.getLength()));
            SnakesProto.GameMessage.AnnouncementMsg ann = msgPB.getAnnouncement();
            players = new ArrayList<>();
            for (SnakesProto.GamePlayer pl: ann.getPlayers().getPlayersList()){
                players.add(new Player(pl));
            }
            this.config = new Config(ann.getConfig());
            this.canJoin = ann.getCanJoin();
            this.serverAddress = new InetSocketAddress(msg.getAddress(), msg.getPort());
        } catch (InvalidProtocolBufferException e) {
            System.out.println("Can't parse announcement message!");
        }
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public Config getConfig() {
        return config;
    }

    public boolean isCanJoin() {
        return canJoin;
    }

    public InetSocketAddress getServerAddress() {
        return serverAddress;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ServerToJoin && this.getServerAddress().equals(((ServerToJoin)obj).getServerAddress())
                && this.isCanJoin() == ((ServerToJoin)obj).isCanJoin();
    }
}
