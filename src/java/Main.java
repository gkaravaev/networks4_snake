import client.ClientThread;
import utils.wrappers.Config;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void startSnake(ArrayList<Number> numbers, ArrayList<String> name) {
        Config cf = new Config();
        cf.setWidth((Integer) numbers.get(0));
        cf.setHeight((Integer) numbers.get(1));
        cf.setFoodStatic((Integer) numbers.get(2));
        cf.setFoodPerPlayer((Float) numbers.get(3));
        cf.setStateMs((Integer) numbers.get(4));
        cf.setDeadFoodProb((Float) numbers.get(5));
        cf.setPingMs((Integer) numbers.get(6));
        cf.setTimeoutMs((Integer) numbers.get(7));
        ClientThread client = new ClientThread(name.get(0), (Integer)numbers.get(numbers.size() - 1), cf);
        Thread clientThread = new Thread(client);
        ClientView view = new ClientView(client);
        clientThread.start();
        view.launchClient();
    }

    public static void main(String[] args) {
        /*StartWindow sw = new StartWindow();
        sw.makeStartWindow(new ArrayList<>(Arrays.asList(40, 30, 1, 1.0, 1000, 0.1, 100, 5000, 8080)), new ArrayList<>(Arrays.asList("name")));*/
        ClientThread client = new ClientThread(args[0], Integer.parseInt(args[1]), new Config());
        Thread clientThread = new Thread(client);
        ClientView view = new ClientView(client);
        clientThread.start();
        view.launchClient();
    }
}