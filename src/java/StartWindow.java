import client.ClientThread;
import utils.wrappers.Config;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;



public class StartWindow {
    private ArrayList<JTextField> textFields;
    private GridBagConstraints gbc;
    private JFrame frame;
    private void addTextBox(String def, int y){
        gbc.gridy = y;
        textFields.add(new JTextField(def));
        textFields.get(y).setEditable(true);
        frame.add(textFields.get(y), gbc);
    }

    public void makeStartWindow(ArrayList<Number> numbers, ArrayList<String> name) {
        frame = new JFrame();
        GridBagLayout grid = new GridBagLayout();
        gbc = new GridBagConstraints();
        frame.setLayout(grid);
        frame.setTitle("Settings");
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 0.1;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        frame.add(new JLabel("Width (from 10 to 100)"), gbc);
        gbc.gridy = 1;
        frame.add(new JLabel("Height (from 10 to 100)"), gbc);
        gbc.gridy = 2;
        frame.add(new JLabel("Food static (from 0 to 100)"), gbc);
        gbc.gridy = 3;
        frame.add(new JLabel("Food per player (from 0.0 to 100.0)"), gbc);
        gbc.gridy = 4;
        frame.add(new JLabel("State delay ms (from 1 to 10000)"), gbc);
        gbc.gridy = 5;
        frame.add(new JLabel("Dead food prob (from 0 to 1)"), gbc);
        gbc.gridy = 6;
        frame.add(new JLabel("Ping delay ms (from 1 to 10000)"), gbc);
        gbc.gridy = 7;
        frame.add(new JLabel("Node timeout ms (from 1 to 10000"), gbc);
        gbc.gridy = 8;
        frame.add(new JLabel("Port"), gbc);
        gbc.gridy = 9;
        frame.add(new JLabel("Name"), gbc);

        gbc.fill = GridBagConstraints.NONE;
        JButton doneButton = new JButton("Done!");
        gbc.weightx = 0.1;
        gbc.gridy = 10;
        gbc.gridx = 0;
        frame.add(doneButton, gbc);
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 1;
        gbc.ipadx = 30;
        gbc.ipady = 5;
        textFields = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            addTextBox(numbers.get(i).toString(), i);
        }
        name.add("Name");
        addTextBox(name.get(0), 9);
        doneButton.addActionListener(e -> {
            int i = 0;
            for (JTextField field : textFields) {
                String text = field.getText();
                Integer result;
                try {
                    if (i == 3 || i == 5) {
                     numbers.set(i, Float.parseFloat(text));
                    } else if (i == 9) {
                        name.set(0, text);
                    } else {
                        result = Integer.parseInt(text);
                        numbers.set(i, result);
                    }
                    i++;
                } catch (Exception e1) {
                    doneButton.setText("Error!");
                    break;
                }
            }
            if (i == textFields.size()) {
                try {
                    frame.setVisible(false);
                    frame.dispose();
                    Main.startSnake(numbers, name);
                    /*Config cf = new Config();
                    cf.setWidth((Integer) numbers.get(0));
                    cf.setHeight((Integer) numbers.get(1));
                    cf.setFoodStatic((Integer) numbers.get(2));
                    cf.setFoodPerPlayer((Float) numbers.get(3));
                    cf.setStateMs((Integer) numbers.get(4));
                    cf.setDeadFoodProb((Float) numbers.get(5));
                    cf.setPingMs((Integer) numbers.get(6));
                    cf.setTimeoutMs((Integer) numbers.get(7));*/
                    return;
                } catch (Exception e1) {
                    doneButton.setText("Error!");
                }
            }
        });

        frame.setSize(550, 600);
        frame.setPreferredSize(frame.getSize());
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

}
