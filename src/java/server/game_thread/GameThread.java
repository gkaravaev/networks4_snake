package server.game_thread;

import snakes.SnakesProto;
import utils.MutableInt;
import utils.NotConfirmedMessage;
import utils.UDatagramSocket;
import utils.wrappers.*;
import utils.wrappers.message_wrappers.MState;
import utils.wrappers.message_wrappers.Role;

import javax.management.monitor.Monitor;
import java.io.IOException;
import java.lang.management.MonitorInfo;
import java.net.*;
import java.util.*;

/**
 * Поток, вычисляющий всю игровую логику, такую как: <p>
 * - Передвижение змей; <p>
 * - Поедание змеями еды (и рост после этого); <p>
 * - Столкновение змей; <p>
 * И рассылающий игрокам GameState каждые delay_ms.
 */
public class GameThread implements Runnable {
    private Config config;
    private State state;
    private UDatagramSocket gameThreadSocket;
    private MutableInt msgSeq;
    private HashMap<Long, NotConfirmedMessage> messages;

    public GameThread(Config config, State state, MutableInt msgSeq, HashMap<Long, NotConfirmedMessage> messages, UDatagramSocket socket) {
        this.config = config;
        this.state = state;
        this.gameThreadSocket = socket;
        this.msgSeq = msgSeq;
        this.messages = messages;
    }

    public GameThread(Config config, State state, MutableInt msgSeq, int port) {
        this.messages = new HashMap<>();
        this.config = config;
        this.state = state;
        try {
            this.gameThreadSocket = new UDatagramSocket(new DatagramSocket(port));
        } catch (SocketException e) {
            System.out.println("Error creating game thread socket!");
            Thread.currentThread().interrupt();
        }
        this.msgSeq = msgSeq;
    }

    public Config getConfig() {
        return config;
    }

    public State getState() {
        return state;
    }

    public ArrayList<Player> getPlayers() {
        return state.getPlayers();
    }

    public ArrayList<State.Snake> getSnakes() {
        return state.getSnakes();
    }

    public ArrayList<State.Coord> getFood() {
        return state.getFoods();
    }

    public Cell[][] getField() {
        return state.getField();
    }

    private int getMasterId() {
        for (Player pl : state.getPlayers()) {
            if (pl.getRole().equals(NodeRole.MASTER)) {
                return pl.getId();
            }
        }
        throw new NoSuchElementException("No admin??");
    }

    private Player getPlayerByID(int id) {
        for (Player pl : state.getPlayers()) {
            if (pl.getId() == id) {
                return pl;
            }
        }
        throw new NoSuchElementException("No such player");
    }

    private void trimCoord(State.Coord coord) {
        if (coord.getX() < 0)
            coord.setX(config.getWidth() + coord.getX());
        else if (coord.getX() >= config.getWidth())
            coord.setX(coord.getX() % config.getWidth());

        if (coord.getY() < 0)
            coord.setY(config.getHeight() + coord.getY());
        else if (coord.getY() >= config.getHeight())
            coord.setY(coord.getY() % config.getHeight());

    }

    private void spawnFoodInRandomCell() {
        int randX;
        int randY;
        do {
            randX = new Random().nextInt(config.getWidth());
            randY = new Random().nextInt(config.getHeight());
        } while (!state.getField()[randX][randY].equals(Cell.Empty));
        state.getField()[randX][randY] = Cell.Food;
        state.getFoods().add(new State.Coord(randX, randY));
    }

    // убиваем змейку, каждая клетка с вероятностью config.getDeadFoodProb() превращается в еду
    private void killSnake(State.Snake snake) {
        State.Coord snakePoint = snake.getPoints().get(0);
        State.Coord itCoord = new State.Coord(snakePoint.getX(), snakePoint.getY());
        for (int i = 1; i < snake.getPoints().size(); i++) {
            itCoord.setX(itCoord.getX() + snake.getPoints().get(i).getX());
            itCoord.setY(itCoord.getY() + snake.getPoints().get(i).getY());
            trimCoord(itCoord);
            spawnFoodOnDeadSnake(itCoord);
        }
        // удаляем змейку, посылаем игроку сообщение что он умер
        state.getSnakes().remove(snake);
        if (snake.getState().equals(SnakesProto.GameState.Snake.SnakeState.ALIVE)) {
            Player deadPlayer = getPlayerByID(snake.getPlayerID());
            getPlayerByID(snake.getPlayerID()).setRole(NodeRole.VIEWER);
            // если умер мастер, то нет смысла отправлять ему же об этом сообщение,
            // его игровой клиент берет данные напрямую из треда на той же машине
            if (!deadPlayer.getRole().equals(NodeRole.MASTER)) {
                GameMessage deathMessage = new GameMessage(MessageType.ROLE, (long) msgSeq.getInt(),
                        getMasterId(), snake.getPlayerID(), new Role(NodeRole.MASTER, NodeRole.VIEWER),
                        new InetSocketAddress(deadPlayer.getIp(), deadPlayer.getPort()));
                try {
                    gameThreadSocket.sendGMessage(deathMessage);
                    messages.put((long) msgSeq.getInt(), new NotConfirmedMessage(deathMessage, new Date().getTime()));
                } catch (UnknownHostException e) {
                    System.out.println("Can't get dead player's IP address!");
                } catch (IOException e) {
                    System.out.println("Can't send death message!");
                }
                msgSeq.incInt(1);
            }
        }

    }


    private void spawnFoodOnDeadSnake(State.Coord snakePoint) {
        if (new Random().nextDouble() < config.getDeadFoodProb()) {
            state.getField()[snakePoint.getX()][snakePoint.getY()] = Cell.Food;
            state.getFoods().add(new State.Coord(snakePoint.getX(), snakePoint.getY()));
        } else {
            state.getField()[snakePoint.getX()][snakePoint.getY()] = Cell.Empty;
        }
    }

    // наконец передвинем голову змейки уже на поле (field),
    // если столкновение - змейку убиваем
    private void checkCollisions() {
        ArrayList<State.Snake> snakesToKill = new ArrayList<>();
        for (State.Snake snake : getSnakes()) {
            State.Coord snakeHead = snake.getPoints().get(0);
            if (state.getField()[snakeHead.getX()][snakeHead.getY()] == Cell.Snake) {
                snakesToKill.add(snake);
            } else {
                state.getField()[snakeHead.getX()][snakeHead.getY()] = Cell.Snake;
            }
        }
        for (State.Snake snake : snakesToKill) {
            killSnake(snake);
            System.out.println("Killed snake " + snake.getPlayerID());
        }
    }


    private State.Coord getSnakeTailCoord(State.Snake snake) {
        State.Coord ans = new State.Coord(snake.getPoints().get(0).getX(), snake.getPoints().get(0).getY());
        for (int i = 1; i < snake.getPoints().size(); i++) {
            ans.setX(ans.getX() + snake.getPoints().get(i).getX());
            ans.setY(ans.getY() + snake.getPoints().get(i).getY());
        }
        trimCoord(ans);
        return ans;
    }

    // протокол предполагает что змейки из 1 точки быть не может
    // получается, что движение змейки из протокола - это передвигаем голову,
    // а остальные точки передвигаем по принципу:
    // смещение i+1-ой точки = смещение i-ой точки
    // внимание! изменение смещения 1-ой точки (идущей сразу после головы) должно поступать от сервера.
    private void moveSnake(State.Snake snake) {
        boolean isGrowing = false;
        ArrayList<State.Coord> snakePoints = snake.getPoints();
        State.Coord currentPoint = snakePoints.get(0);
        State.Coord snakeTail = getSnakeTailCoord(snake);
        currentPoint.setX(currentPoint.getX() + snake.getDirection().toCoord().getX());
        currentPoint.setY(currentPoint.getY() + snake.getDirection().toCoord().getY());
        trimCoord(currentPoint);


        if (state.getField()[currentPoint.getX()][currentPoint.getY()] == Cell.Food) {
            isGrowing = true;
        }

        // мы запомнили передвижение змейки в самой змейке,
        // но сначала передвинем хвосты всех змеек, а потом уже проверим кто в кого врезался
        //field[newX][newY] = Cell.Snake;

        int lastPointX = snakePoints.get(snakePoints.size() - 1).getX();
        int lastPointY = snakePoints.get(snakePoints.size() - 1).getY();

        // передвигаем все точки змейки от 2ой до хвоста (size - 1)
        // 1ая всегда зависит от головы, голову (0) передвигает сервер

        for (int i = snake.getPoints().size() - 1; i > 1; i--) {
            currentPoint = snakePoints.get(i);
            currentPoint.setX(snakePoints.get(i - 1).getX());
            currentPoint.setY(snakePoints.get(i - 1).getY());
        }

        snakePoints.set(1, snake.getDirection().getReverse().toCoord());
        // если змейка не выросла то хвост сдвинулся и клетка стала свободной
        if (!isGrowing)
            state.getField()[snakeTail.getX()][snakeTail.getY()] = Cell.Empty;
            // если змейка выросла то растет она с хвоста и поэтому мы добавляем новую клетку в конец => хвост не двигается
        else {
            State.Coord newPoint = new State.Coord(lastPointX, lastPointY);
            snake.getPoints().add(newPoint);
            state.getFoods().removeIf(o -> o.equals(snakePoints.get(0)));
            if (snake.getState().equals(SnakesProto.GameState.Snake.SnakeState.ALIVE))
                getPlayerByID(snake.getPlayerID()).setScore(1 + getPlayerByID(snake.getPlayerID()).getScore());
        }
        // менять state не надо, т.к. мы работали с обертками (одним и тем же объектом)
    }

    // рассылаем игрокам состояние поля чтобы они его могли отрисовать
    private void sendStateToPlayers() {
        for (Player player : state.getPlayers()) {
            // мастеру нет смысла это отправлять потому что его игровой клиент state получает
            // прямо из этого треда
            if (!player.getRole().equals(NodeRole.MASTER)) {
                try {
                    GameMessage gm = new GameMessage(MessageType.STATE,
                            (long) msgSeq.getInt(), new MState(state), new InetSocketAddress(InetAddress.getByName(player.getIp()), player.getPort()));
                    gameThreadSocket.sendGMessage(gm);
                    messages.put((long) msgSeq.getInt(), new NotConfirmedMessage(gm, new Date().getTime()));
                } catch (UnknownHostException e) {
                } catch (IOException e) {
                    System.out.println("Error sending game state!");
                }
                msgSeq.incInt(1);
            }
        }

    }

    private void initField() {
        for (int i = 0; i < config.getWidth(); i++) {
            for (int j = 0; j < config.getHeight(); j++) {
                state.getField()[i][j] = Cell.Empty;
            }
        }
        for (State.Coord coord : state.getFoods()) {
            state.getField()[coord.getX()][coord.getY()] = Cell.Food;
        }
        for (int i = state.getFoods().size(); i < Math.floor(state.getConfig().getFoodPerPlayer() * getPlayers().size()) + state.getConfig().getFoodStatic(); i++)
            spawnFoodInRandomCell();
        for (State.Snake snake : state.getSnakes()) {
            State.Coord snakeHead = snake.getPoints().get(0);
            State.Coord itCoord = new State.Coord(snakeHead.getX(), snakeHead.getY());
            state.getField()[itCoord.getX()][itCoord.getY()] = Cell.Snake;
            for (int i = 1; i < snake.getPoints().size(); i++) {
                State.Coord currentPoint = snake.getPoints().get(i);
                itCoord.setX(itCoord.getX() + currentPoint.getX());
                itCoord.setY(itCoord.getY() + currentPoint.getY());
                trimCoord(itCoord);

                state.getField()[itCoord.getX()][itCoord.getY()] = Cell.Snake;
            }
        }
    }

    @Override
    public void run() {
        synchronized (state) {
            initField();
        }
        while (true) {
            try {
                Thread.sleep(config.getStateMs());
            } catch (InterruptedException e) {
                System.out.println("Thread was interrupted!");
            }
            // дофига использования штук внутри state поэтому просто всё синхронизируем по нему
            synchronized (state) {
                for (State.Snake snake : state.getSnakes()) {
                    moveSnake(snake);
                }
                checkCollisions();
                if (state.getFoods().size() < Math.floor(state.getConfig().getFoodPerPlayer() * getPlayers().size()) + state.getConfig().getFoodStatic()) {
                    for (int i = 0; i < Math.floor(state.getConfig().getFoodPerPlayer() * getPlayers().size()) + state.getConfig().getFoodStatic() - state.getFoods().size(); i++) {
                        spawnFoodInRandomCell();
                    }
                }
                state.setStateOrder(state.getStateOrder() + 1);
                sendStateToPlayers();
            }
        }
    }
}
