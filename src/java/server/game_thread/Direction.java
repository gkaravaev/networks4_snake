package server.game_thread;

import snakes.SnakesProto;
import utils.wrappers.State;

public enum Direction {
    UP, RIGHT, DOWN, LEFT;


    public State.Coord toCoord() {
        switch (this) {
            case UP:
                return new State.Coord(0, -1);
            case RIGHT:
                return new State.Coord(1, 0);
            case DOWN:
                return new State.Coord(0, 1);
            case LEFT:
                return new State.Coord(-1, 0);
            default:
                return new State.Coord(0, 0);
        }
    }

    public Direction getReverse() {
        switch (this) {
            case UP:
                return DOWN;
            case RIGHT:
                return LEFT;
            case DOWN:
                return UP;
            case LEFT:
                return RIGHT;
            default:
                return DOWN;
        }
    }

    public static Direction fromProtobuf(SnakesProto.Direction pb) {
        switch (pb) {
            case UP:
                return UP;
            case DOWN:
                return DOWN;
            case LEFT:
                return LEFT;
            case RIGHT:
                return RIGHT;
            default:
                return RIGHT;
        }
    }

    public SnakesProto.Direction toProtobuf() {
        switch (this) {
            case UP:
                return SnakesProto.Direction.UP;
            case DOWN:
                return SnakesProto.Direction.DOWN;
            case LEFT:
                return SnakesProto.Direction.LEFT;
            case RIGHT:
                return SnakesProto.Direction.RIGHT;
            default:
                return SnakesProto.Direction.RIGHT;
        }
    }
}
