package server.network_thread;

import utils.MutableInt;
import utils.UDatagramSocket;
import utils.wrappers.Config;
import utils.wrappers.GameMessage;
import utils.wrappers.MessageType;
import utils.wrappers.State;
import utils.wrappers.message_wrappers.Announcement;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.NoSuchElementException;
import java.util.TimerTask;

public class MulticastTimerTask extends TimerTask {

    private UDatagramSocket multicastSocket;
    private Config config;
    private State gameState;
    private MutableInt msgSeq;

    MulticastTimerTask(UDatagramSocket multicastSocket, Config config, State gameState, MutableInt msgSeq) {
        this.config = config;
        this.multicastSocket = multicastSocket;
        this.gameState = gameState;
        this.msgSeq = msgSeq;
    }

    @Override
    public void run() {
        boolean canJoin = true;
        synchronized (gameState) {
            try {
                gameState.findPlaceForNewSnake();
            } catch (NoSuchElementException e) {
                canJoin = false;
            }
        }
            GameMessage annMsg = new GameMessage(MessageType.ANNOUNCEMENT, 0L,
                    new Announcement(gameState.getPlayers(), config, canJoin),
                    new InetSocketAddress("239.192.0.4", 9192));
            try {
                multicastSocket.sendGMessage(annMsg);
            } catch (IOException e) {
                System.out.println("Error in multicast task!");
            }
            msgSeq.incInt(1);


    }
}
