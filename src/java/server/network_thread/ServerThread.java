package server.network_thread;

import client.ClientThread;
import server.game_thread.GameThread;
import utils.MutableInt;
import utils.NotConfirmedMessage;
import utils.UDatagramSocket;
import utils.wrappers.*;
import utils.wrappers.message_wrappers.Ack;
import utils.wrappers.message_wrappers.Error;
import utils.wrappers.message_wrappers.Join;
import utils.wrappers.message_wrappers.Role;
import utils.wrappers.message_wrappers.Steer;

import java.io.IOException;
import java.net.*;
import java.util.*;

public class ServerThread implements Runnable {

    // таймер + таймер таск для постоянной рассылки состояния сервера в мультикаст
    private MulticastTimerTask mcSender;
    private Timer mcTimer;

    // каждый сервер это одновременно и клиент
    private ClientThread myClient;

    // udp сокет для отправки сообщений
    private UDatagramSocket serverSocket;

    // таблицы для хранения таймаута игроков и сообщений от игроков
    private HashMap<Integer, Long> playersTimeout; //id, getTime
    private HashMap<Long, NotConfirmedMessage> messagesTimeout; //msgSeq, message

    // различные переменные сервера - нужны для рассылки и прочего
    private final State state;
    private Config config;
    private MutableInt msgSeq;
    private int playerIDcounter;

    // запустить новый сервер
    public ServerThread(UDatagramSocket socket, Config config, ClientThread myClient) {
        this.myClient = myClient;
        this.serverSocket = socket;
        this.playersTimeout = new HashMap<>();
        this.messagesTimeout = new HashMap<>();
        this.config = config;
        ArrayList<Player> players = new ArrayList<>();
        Player admin = new Player();
        admin.setId(0);
        admin.setRole(NodeRole.MASTER);
        try {
            admin.setIp(InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        admin.setPort(socket.getSocket().getLocalPort());
        admin.setName("admin");
        players.add(admin);
        this.state = new State(config);
        this.state.setPlayers(players);
        this.msgSeq = new MutableInt();
        this.mcSender = new MulticastTimerTask(serverSocket, config, state, msgSeq);
        this.playerIDcounter = 1;
        state.spawnSnakeInSquare(state.findPlaceForNewSnake(), 0);
    }

    // подхватить старый сервер
    public ServerThread(UDatagramSocket socket, Config config, State state, ClientThread myClient) {
        for (State.Snake s : state.getSnakes()) {
            System.out.println(s.getPlayerID());
        }
        this.myClient = myClient;
        this.serverSocket = socket;
        this.playersTimeout = new HashMap<>();
        this.messagesTimeout = new HashMap<>();
        this.config = config;
        this.state = state;
        this.msgSeq = new MutableInt();
        this.mcSender = new MulticastTimerTask(serverSocket, config, state, msgSeq);
        this.playerIDcounter = 0;
        this.state.kickPlayer(state.findIDofRole(NodeRole.MASTER));
        this.state.getPlayerByID(myClient.getMyID()).setRole(NodeRole.MASTER);
        for (Player p : state.getPlayers()) {
            if (p.getId() != myClient.getMyID()) {
                try {
                    if (p.getId() > this.playerIDcounter)
                        this.playerIDcounter = p.getId();
                    GameMessage imMaster = new GameMessage(MessageType.ROLE, (long) msgSeq.getInt(),
                            state.findIDofRole(NodeRole.MASTER), p.getId(),
                            new Role(NodeRole.MASTER, NodeRole.NORMAL),
                            new InetSocketAddress(p.getIp(), p.getPort()));
                    serverSocket.sendGMessage(imMaster);
                    playersTimeout.put(p.getId(), new Date().getTime());
                    messagesTimeout.put((long) msgSeq.getInt(),
                            new NotConfirmedMessage(imMaster, new Date().getTime()));
                    msgSeq.incInt(1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public State getState() {
        return state;
    }


    //послать соообщение об ошибке в ответ на сообщение msgToErr
    private void sendErrorMessage(GameMessage msgToErr, String error) {
        Player receiver = state.getPlayerByID(msgToErr.getSenderID());
        GameMessage errMsg = new GameMessage(MessageType.ERROR,
                (long) msgSeq.getInt(), msgToErr.getReceiverID(), msgToErr.getSenderID(),
                new Error(error), new InetSocketAddress(receiver.getIp(), receiver.getPort()));
        try {
            serverSocket.sendGMessage(errMsg);
        } catch (IOException e) {
            System.out.println("Failed to send error message!");
        }
    }

    // послать сообщение об успешной доставке игроку с id receiverID
    private void sendConfirmationMessage(GameMessage msgToConfirm, int receiverID) {
        GameMessage ackMsg = new GameMessage(MessageType.ACK,
                msgToConfirm.getMsgSeq(), msgToConfirm.getReceiverID(), receiverID, new Ack(),
                msgToConfirm.getAddress());
        try {
            serverSocket.sendGMessage(ackMsg);
        } catch (IOException e) {
            System.out.println("Failed to send confirmation message!");
        }
    }


    // обработать пришедшее сообщение
    private void processMessage(GameMessage message) {
        System.out.println("Got message of type " + message.getMessageType().toString()); //debug
        switch (message.getMessageType()) {
            // игрок сигнализирует что живой
            case PING:
                playersTimeout.put(message.getSenderID(), new Date().getTime());
                sendConfirmationMessage(message, message.getSenderID());
                break;
            // игрок сигнализирует что он поворачивает - включает в себя PING
            case STEER:
                synchronized (state) {
                    if (!state.getPlayerByID(message.getSenderID()).getRole().equals(NodeRole.VIEWER))
                        state.steerSnake(message.getSenderID(),
                                ((Steer)(message.getMsgContent())).getDirection());
                }
                playersTimeout.put(message.getSenderID(), new Date().getTime());
                sendConfirmationMessage(message, message.getSenderID());
                break;
            // подтверждение доставки сообщения
            case ACK:
                messagesTimeout.remove(message.getMsgSeq());
                break;
            // подключается новый игрок
            case JOIN:
                // создаем для него сущность, которую мы поместим в state
                Player newPlayer = new Player(message, playerIDcounter);
                playerIDcounter++;

                synchronized (state) {
                    state.getPlayers().add(newPlayer);
                    if (!(((Join) message.getMsgContent()).isOnlyView())) {
                        // если игрок подключается для игры, то
                        try {
                            // находим ему место для спавна
                            State.Coord upperLeft = state.findPlaceForNewSnake();
                            // спавним
                            state.spawnSnakeInSquare(upperLeft, newPlayer.getId());
                            // если нет заместителя, то назначаем его заместителем, иначе - просто игрок
                            if (state.isDeputyExists())
                                newPlayer.setRole(NodeRole.NORMAL);
                            else {
                                newPlayer.setRole(NodeRole.DEPUTY);
                            }
                        } catch (NoSuchElementException e) {
                            // места нет, отправляем сообщение об ошибке
                            sendErrorMessage(message, "No place to join!");
                        }
                    } else {
                        // игрок подключился просто посмотреть
                        if (state.isDeputyExists())
                            newPlayer.setRole(NodeRole.VIEWER);
                        else {
                            newPlayer.setRole(NodeRole.DEPUTY);
                        }
                    }
                }
                playersTimeout.put(newPlayer.getId(), new Date().getTime());
                sendConfirmationMessage(message, newPlayer.getId());
                // если игрок был назначен заместителем, то надо об этом ему сказать
                // - он сейчас знает только что он к серверу присоединился
                if (newPlayer.getRole().equals(NodeRole.DEPUTY)) {
                    GameMessage deputyMsg = new GameMessage(MessageType.ROLE, (long) msgSeq.getInt(),
                            state.findIDofRole(NodeRole.MASTER), newPlayer.getId(),
                            new Role(NodeRole.MASTER, NodeRole.DEPUTY), message.getAddress());
                    try {
                        serverSocket.sendGMessage(deputyMsg);
                    } catch (IOException e) {
                        System.out.println("Deputy message not sent!");
                    }
                    messagesTimeout.put((long) msgSeq.getInt(), new NotConfirmedMessage(deputyMsg, new Date().getTime()));
                    msgSeq.incInt(1);
                }
                break;
            case ROLE:
                if (((Role) message.getMsgContent()).getReceiverRole().equals(NodeRole.VIEWER)) {
                    // 3. от главного к умершему игроку (receiver_role = VIEWER)
                    // сервер сам себе послал инфу о том что он умер, сам же прочитал и сам же себя и убил
                    myClient.setIsPlaying(false);
                }
                // 2. от осознанно выходящего игрока (sender_role = VIEWER)
                if (((Role) message.getMsgContent()).getReceiverRole() == NodeRole.MASTER
                        && ((Role) message.getMsgContent()).getSenderRole() == NodeRole.VIEWER) {
                    synchronized (state) {
                        Player player = state.getPlayerByID(message.getSenderID());
                        // делаем его viewer-ом, удаляем его змейку
                        player.setRole(NodeRole.VIEWER);
                        state.kickPlayer(player.getId());
                    }
                }
                playersTimeout.put(message.getSenderID(), new Date().getTime());
                sendConfirmationMessage(message, message.getSenderID());
                break;
            default:
                // остальные сообщения до нас доходить не должны
                System.out.println("Server got an unsupported message! Something went wrong.");
                break;
        }
    }

    // вырубаем сервер : передаем управление заместителю, вырубаем наш мультикаст таймер, вырубаем сам тред
    public void stopServer() {
        synchronized (state) {
            if (state.isDeputyExists()) {
                Player deputy = state.getPlayerByID(state.findIDofRole(NodeRole.DEPUTY));
                GameMessage quitMessage = new GameMessage(MessageType.ROLE, 0L,
                        state.findIDofRole(NodeRole.MASTER), deputy.getId(),
                        new Role(NodeRole.DEPUTY, NodeRole.MASTER),
                        new InetSocketAddress(deputy.getIp(), deputy.getPort()));
                try {
                    serverSocket.sendGMessage(quitMessage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        mcTimer.cancel();
        Thread.currentThread().interrupt();
    }

    private void checkTimeout(){
        ArrayList <Integer> toRemove = new ArrayList<>();
        for (Map.Entry<Integer, Long> p : playersTimeout.entrySet()) {
            if (new Date().getTime() - p.getValue() > config.getTimeoutMs()) {
                toRemove.add(p.getKey());
                synchronized (state) {
                    System.out.println("Timeout: player with ID " + p.getKey());
                    state.kickPlayer(p.getKey());
                }
            }
        }
        for (Integer i : toRemove) {
            playersTimeout.remove(i);
        }
        ArrayList <Long> toRemove2 = new ArrayList<>();
        for (Map.Entry<Long, NotConfirmedMessage> m : messagesTimeout.entrySet()) {
            if (new Date().getTime() - m.getValue().getTime() > config.getTimeoutMs()) {
                toRemove2.add(m.getKey());
                synchronized (state) {
                    System.out.println("Message timeout: player with ID " + m.getValue().getMessage().getReceiverID());;
                    state.kickPlayer(m.getValue().getMessage().getReceiverID());
                }
            }
        }
        for (Long l : toRemove2) {
            messagesTimeout.remove(l);
        }
    }


    // основной метод - серверный цикл

    @Override
    public void run() {
        // запускаем тред с игровой логикой - обновляет состояние поля, посылает state сообщения
        Thread gameThread = new Thread(new GameThread(config, state, msgSeq, messagesTimeout, serverSocket));
        gameThread.start();
        try {
            serverSocket.setTimeout(1000);
        } catch (SocketException e) {
            System.out.println("Socket timeout error!");
            Thread.currentThread().interrupt();
        }
        // запускаем мультикаст таймер
        mcTimer = new Timer();
        mcTimer.schedule(mcSender, 1000, 1000);
        while (true) {
            try {
                // считываем сообщение, обрабатываем
                GameMessage gameMessage = serverSocket.getGMessage();
                processMessage(gameMessage);
            } catch (SocketTimeoutException e) {
            } catch (IOException e) {
                System.out.println("Socket sending error: " + e.getMessage());
                stopServer();
            }
            checkTimeout();
        }
    }
}
